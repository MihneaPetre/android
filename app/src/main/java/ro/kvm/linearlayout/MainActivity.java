package ro.kvm.linearlayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    public void onClick(View v){
        EditText txtCod=findViewById(R.id.txtCode);
        EditText txtPass=findViewById(R.id.txtNewPass);
        EditText txtConfPass=findViewById(R.id.txtConfirmPass);

        if(!txtCod.getText().toString().isEmpty() && !txtPass.getText().toString().isEmpty()&& !txtConfPass.getText().toString().isEmpty()) {

            Toast.makeText(this, "Password Reseted!", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(this, "Please fill all fields!", Toast.LENGTH_LONG).show();
        }


    }
}
