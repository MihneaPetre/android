package ro.kvm.linearlayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Questions extends AppCompatActivity {
    public static int numar=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);

    }

    public void click(View v)
    {
        ScrollView main=findViewById(R.id.mainLayout);
        LinearLayout layout2=new LinearLayout(getApplicationContext());
        LinearLayout lin=findViewById(R.id.lin);
        layout2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        layout2.setOrientation(LinearLayout.VERTICAL);
        EditText edt=new EditText(getApplicationContext());
        edt.setLayoutParams((new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)));
        edt.setHint("Intrebarea "+ numar);
        EditText edt2=new EditText(getApplicationContext());
        edt2.setLayoutParams((new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)));
        edt2.setHint("Raspuns 1");
        EditText edt3=new EditText(getApplicationContext());
        edt3.setLayoutParams((new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)));
        edt3.setHint("Raspuns 2");
        EditText edt4=new EditText(getApplicationContext());
        edt4.setLayoutParams((new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)));
        edt4.setHint("Raspuns 3");
        EditText edt5=new EditText(getApplicationContext());
        edt5.setLayoutParams((new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)));
        edt5.setHint("Raspuns 4");
        lin.addView(layout2);
        layout2.addView(edt);
        layout2.addView(edt2);
        layout2.addView(edt3);
        layout2.addView(edt4);
        layout2.addView(edt5);
        numar++;
    }
}
